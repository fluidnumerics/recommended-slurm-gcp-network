# Recommended Slurm-GCP Network

This repository contains deployment-manager and terraform scripts for creating the network for hosting the Fluid Numerics Slurm-GCP deployment and other HPC resources (e.g. Lustre file system)

## Getting Started

### Deployment-Manager

```
$ gcloud config set project [PROJECT-ID]
$ cd deployment-manager
$ gcloud deployment-manager deployments create fluid-network --config=fluid-network.yaml
``` 

This add the following resources to your GCP project
```
NAME                                      TYPE                   STATE       ERRORS  INTENT
fluid-network-all-internal-firewall-rule  compute.v1.firewall    IN_PREVIEW  []      CREATE_OR_ACQUIRE
fluid-network-network                     compute.v1.network     IN_PREVIEW  []      CREATE_OR_ACQUIRE
fluid-network-router                      compute.v1.router      IN_PREVIEW  []      CREATE_OR_ACQUIRE
fluid-network-ssh-firewall-rule           compute.v1.firewall    IN_PREVIEW  []      CREATE_OR_ACQUIRE
fluid-network-subnet                      compute.v1.subnetwork  IN_PREVIEW  []      CREATE_OR_ACQUIRE
```

